import { NgModule } from '@angular/core';
import {
  MatButtonModule, MatButtonToggleModule, MatIconModule, MatFormFieldModule, MatInputModule,
  MatSelectModule, MatCardModule, MatSidenavModule, MatBottomSheetModule, MatListModule,
  MatTabsModule, MatMenuModule, MatExpansionModule
} from '@angular/material';

@NgModule({
  imports: [
    MatButtonModule,
    MatButtonToggleModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    MatSidenavModule,
    MatBottomSheetModule,
    MatListModule,
    MatTabsModule,
    MatMenuModule,
    MatExpansionModule
  ],
  exports: [
    MatButtonModule,
    MatButtonToggleModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    MatSidenavModule,
    MatBottomSheetModule,
    MatListModule,
    MatTabsModule,
    MatMenuModule,
    MatExpansionModule
  ]
})
export class MaterialsModule { }
