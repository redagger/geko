import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, ViewEncapsulation } from '@angular/core';
import {NavBarService} from './nav-bar.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements AfterViewInit, OnInit {
  @ViewChild('appDrawer') appDrawer: ElementRef;

  constructor(
    private mynav: NavBarService
  ) {

  }

  ngOnInit(){

  }

  ngAfterViewInit() {
    this.mynav.appDrawer = this.appDrawer;
  }
}
