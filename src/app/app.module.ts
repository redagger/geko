import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ContainerCustomerComponent } from './container-customer/container-customer.component';
import { ViewCustomerComponent } from './container-customer/view-customer/view-customer.component';
import { ViewAnimalsComponent } from './container-customer/view-animals/view-animals.component';
import { ViewReservationsComponent } from './container-customer/view-reservations/view-reservations.component';
import { SideNavLeftComponent } from './side-nav-left/side-nav-left.component';
import { MenuTopComponent } from './menu-top/menu-top.component';
import { SideNavRightComponent } from './side-nav-right/side-nav-right.component';
import { AppRoutingModule } from './/app-routing.module';
import { MaterialsModule } from './materials/materials.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavBarService } from './nav-bar.service';
import { HttpClientModule } from "@angular/common/http";
import { OverviewComponent } from './overview/overview.component';

@NgModule({
  declarations: [
    AppComponent,
    ContainerCustomerComponent,
    ViewCustomerComponent,
    ViewAnimalsComponent,
    ViewReservationsComponent,
    SideNavLeftComponent,
    MenuTopComponent,
    SideNavRightComponent,
    OverviewComponent
  ],
  imports: [
    MaterialsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [NavBarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
