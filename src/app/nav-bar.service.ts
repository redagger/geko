import { EventEmitter, Injectable } from '@angular/core';

@Injectable()
export class NavBarService {

  public appDrawer: any;


  constructor() { }

  public toggleNav() {
    this.appDrawer.toggle();
  }
}
