import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerCustomerComponent } from './container-customer.component';

describe('ContainerCustomerComponent', () => {
  let component: ContainerCustomerComponent;
  let fixture: ComponentFixture<ContainerCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
